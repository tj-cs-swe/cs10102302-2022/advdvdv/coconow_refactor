import os
import json
import random
import coconow.settings
from pickle import TRUE
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from accounts.models import Member
from django.core import mail
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication 
from tools import BaseResponse,loginResponse

verify_list={}
class CsrfExemptSessionAuthentication(SessionAuthentication): 
    def enforce_csrf(self, request): 
        return  # To not perform the csrf check previously happening

class Register(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    def post(self, request, format=None):
        data = json.loads(request.body)
        username = data.get("username").strip()
        password = data.get("password").strip()
        email = data.get("email").strip()
        verify_code = data.get("verify_code").strip()
        # empty field
        if not username or not password or not email or username == "" or password=="" or email == "":
            # TODO： 添加自定义函数简化返回
            reseponse=BaseResponse(message="failure",data={"detail":"用户名、密码、邮箱不能为空"})
            return reseponse.get_response
        if email not in verify_list or verify_list[email]!=verify_code:
            reseponse=BaseResponse(message="failure",data={"detail":"验证码错误"},status=400)
            return reseponse.get_response

        # user already exists
        elif User.objects.filter(username=username).exists():
            reseponse=BaseResponse(message="failure",data={"detail":"用户名已存在"})
            return reseponse.get_response

        # email already exists
        elif User.objects.filter(email=email).exists():
            reseponse=BaseResponse(message="failure",data={"detail":"邮箱已存在"})
            return reseponse.get_response

        # success
        else:
            user=User.objects.create_user(username=username, password=password,email=email)
            verify_list.pop(email)

            # TODO: 改掉死路径
            os.system(coconow.settings.ACCOUNT_JPG+str(user.id)+'.jpg')

            reseponse=BaseResponse(message="success")
            return reseponse.get_response


class Verify(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    def post(self, request, format=None):
        data = json.loads(request.body)
        email = data.get("email").strip()
        if User.objects.filter(email=email).exists():
            reseponse=BaseResponse(message="failure",data={"detail":"邮箱已存在"},status=400)
            return reseponse.get_response

        alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
        characters = "".join(random.sample(alphabet, 5))
        # TODO： 使用模板？
        sendemail(str(email),str('欢迎注册coconow'),str('您的验证码是: ')+characters)
        verify_list[email]=characters
        reseponse=BaseResponse(message="success")
        return reseponse.get_response


class ForgetPasswd1(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    def post(self, request, format=None):
        data = json.loads(request.body)
        email = data.get("email").strip()
        if not User.objects.filter(email=email).exists():
            reseponse=BaseResponse(message="failure",data={"detail":"邮箱不存在"})
            return reseponse.get_response

        alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
        characters = "".join(random.sample(alphabet, 5))
        sendemail(str(email),str('coconow修改密码'),str('您的验证码是: ')+characters)
        verify_list[email]=characters
        reseponse=BaseResponse(message="success")
        return reseponse.get_response

class ForgetPasswd2(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    def post(self, request, format=None):
        data = json.loads(request.body)
        email = data.get("email").strip()
        password = data.get("password").strip()
        verify_code = data.get("verify_code").strip()
        if email not in verify_list or verify_list[email]!=verify_code:
            reseponse=BaseResponse(message="failure",data={"detail":"验证码错误"},status=400)
            return reseponse.get_response

        user=User.objects.get(email=email)
        user.set_password(password)
        user.save()
        reseponse=BaseResponse(message="success")
        return reseponse.get_response

class Login(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    def post(self, request, format=None):
        # print(coconow.settings.UPLOAD_TEMPFILE_PATH)
        user = request.user
        if user.is_authenticated:
            reseponse=BaseResponse(message="failure",data={"detail":"已登录用户不能再次登录"})
            return reseponse.get_response
        data = json.loads(request.body)
        username = data.get("username").strip()
        password = data.get("password").strip()
        email= data.get("email").strip()
        
        user = None
        if email=="":
            user=authenticate(username=username,password=password)
            # failure
            if not user:
                reseponse=BaseResponse(message="failure",data={"detail":"用户名/邮箱或密码不正确"})
                return reseponse.get_response

            # success
            else:
                login(request, user)
                request.session["id"] = user.id
                return Response({
                    "message":"success"
                })
        elif username=="":
            if not User.objects.filter(email=email).exists():
                reseponse=BaseResponse(message="failure",data={"detail":"用户名/邮箱或密码不正确"})
                return reseponse.get_response

            user=authenticate(username=User.objects.filter(email=email)[0].username,password=password)
            if not user:
                reseponse=BaseResponse(message="failure",data={"detail":"用户名/邮箱或密码不正确"})
                return reseponse.get_response
            # success
            else:
                login(request, user)
                request.session["id"] = user.id
                reseponse=BaseResponse(message="success")
                return reseponse.get_response

class MyInfo(APIView):
    def get(self, request, format=None):
        user = request.user
        if not user.is_authenticated:
            return loginResponse()

        else:
            id = User.objects.filter(username=user.username).first()
            members = list(Member.objects.filter(uid = id))
            projectsinfo = []
            for membership in members:
                projectsinfo.append({"pid": membership.pid.id, "pname": membership.pid.pname, "privilege": membership.privilege,"color":membership.colour})
            reseponse=BaseResponse(message="success",data={
                    "uid": user.id,
                    "username":user.username,
                    "email":user.email,
                    "projects":projectsinfo,
                })
            return reseponse.get_response


class Logout(APIView):
    def get(self, request, format=None):
        user = request.user
        if not user.is_authenticated:
            return loginResponse()
        else:
            logout(request)
            reseponse=BaseResponse(message="success")
            return reseponse.get_response

class GetIdentity(APIView):
    def get(self, request):
        user = request.user
        if not user.is_authenticated:
            reseponse=BaseResponse(isLogin="False")
            return reseponse.get_response
        else:
            reseponse=BaseResponse(isLogin="True")
            return reseponse.get_response

class UploadAvatar(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)
    def post(self, request):
        # 判断用户登录
        user=request.user
        if not user.is_authenticated:
            return loginResponse()
        else:
            files = request.FILES.getlist('file')
            file_path = os.path.join(os.path.join(coconow.settings.BASE_DIR, 'static/avatar'),str(user.id)+'.jpg')
            for f in files:
                with open(file_path,'wb+') as fp :
                    for chunk in f.chunks():
                        fp.write(chunk)
            reseponse=BaseResponse(message="success")
            return reseponse.get_response    


def sendemail(to,subject,message):
    subject = subject  # 主题
    from_email = coconow.settings.EMAIL_FROM  # 发件人，在settings.py中已经配置
    to_email = to  # 邮件接收者列表
    # 发送的消息
    message = message  # 发送普通的消息使用的时候message
    mail.send_mail(subject, message, from_email, [to_email])
    return TRUE
