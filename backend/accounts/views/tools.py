from rest_framework.response import Response
from django.utils import six
from rest_framework import status

class BaseResponse(Response):
    def __init__(self, message='success', data={}, status=status.HTTP_200_OK,
                 template_name=None, headers=None, exception=False, content_type='application/json',**kwargs):
        super(Response, self).__init__(None, status=status)
        self._message = message
        self._data = data

        self.data = { "message": message, "data": data}
        self.template_name = template_name
        self.exception = exception
        self.content_type = content_type
       
        data.update(kwargs)
       
        if headers:
            for name, value in six.iteritems(headers):
                self[name] = value

    @property
    def message(self):
        return self._message

    @message.setter
    def message(self, value):
        self._message = value

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value

    def get_response(self):
        return Response(self.__dict__)

def loginResponse():
    reseponse=BaseResponse(message="failure",data={"detail":"用户未登录"},status=400)
    return reseponse.get_response