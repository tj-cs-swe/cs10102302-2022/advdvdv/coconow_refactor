from channels.generic.websocket import WebsocketConsumer
import paramiko
import threading
from django.contrib.auth.models import User
from accounts.models import Member, Project
import json
import coconow.settings
import time


class SSHRecvThread(threading.Thread):
    def __init__(self, comsumer):
        threading.Thread.__init__(self)
        self.comsumer = comsumer

    def run(self):
        while True:
            out = self.comsumer.chan.recv(1024)
            # 这里可以不用动
            if len(out) == 0:
                break
            out = out.decode("utf8")
            # print('out',repr(out))
            self.comsumer.send(out)
            # DONE: 睡眠防止抢占CPU
            time.sleep(0.001)


class WebSSHService(WebsocketConsumer):
    def connect(self):
        # To accept the connection call:
        self.accept()
        self.sh = paramiko.SSHClient()  # 1 创建SSH对象
        self.chan = None

    def receive(self, text_data=None, bytes_data=None):
        data = json.loads(text_data)
        if data.get('type') == 'init':
            # DONE: 改用.get方法
            user = User.objects.get(pk=int(data.get('uid')))
            project = Project.objects.get(pk=int(data.get('pid')))
            username = None
            if Member.objects.get(uid=user, pid=project).privilege == 'owner':
                username = "root"
            else:
                username = user.username
            self.sshThreadStart(username, int(data.get('pid')))
        elif data.get('type') == 'opt':
            if self.chan is None:
                # print("webssh error!")
                self.send('no connect!')
            opt = data.get('opt')
            self.chan.send(opt)

    def disconnect(self, code):
        # print('webssh断开连接')
        pass

    # DONE: 提取独立代码片段为函数
    def sshThreadStart(self, username, pid):
        self.sh.set_missing_host_key_policy(
            paramiko.AutoAddPolicy())  # 2 允许连接不在know_hosts文件中的主机
        # DONE: 地址从配置中读取
        self.sh.connect(coconow.settings.SSH_PORT,
                        port=20000 + pid,
                        username=username,
                        password=coconow.settings.SSH_PASSWORD)  # 3 连接服务器
        self.chan = self.sh.invoke_shell(term='xterm')
        self.recvThread = SSHRecvThread(self)
        self.recvThread.setDaemon(True)
        self.recvThread.start()
        self.chan.send('cd /home\n')
        # print("webssh连接成功")
