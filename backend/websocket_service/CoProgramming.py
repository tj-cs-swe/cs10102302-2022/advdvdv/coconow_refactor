from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json
from file.tools import dockeropen, dockersave, removetmp, tmppath  # DONE: 尽量不import*
from threading import BoundedSemaphore
# DONE: 注释无效import
# import time
# from inspect import getfile
# from multiprocessing import managers
# from multiprocessing.pool import ApplyResult
# from tokenize import group
# from turtle import clear

consumerSemaphore = BoundedSemaphore(1)

Manager = {}  # 放打开的project


# ot算法表示保持不变
def isRetain(op):
    return type(op) == int and op > 0


# ot算法表示插入
def isInsert(op):
    return type(op) == str


# ot算法表示删除
def isDelete(op):
    return type(op) == int and op < 0


def applyOperation(pid, path, ops):
    manage = getFileManage(pid, path)
    index = 0
    for op in ops:
        if isRetain(op):
            index += op
        elif isDelete(op):
            manage.content = manage.content[0:index] + manage.content[index -
                                                                      op:]
        elif isInsert(op):
            manage.content = manage.content[0:index] + op + manage.content[
                index:]
    # print(manage.content)


class GroupManage:
    def __init__(self):
        self.group = {}
        self.files = {}
        self.channels = []


class FileManage:
    def __init__(self):
        self.users = {}
        self.content = ""
        self.revision = 0


def getGroupManage(pid):
    if Manager.get(pid) is None:
        Manager[pid] = GroupManage()
    return Manager[pid]


def getFileManage(pid, path):
    GroupManage = getGroupManage(pid)
    if GroupManage.files.get(path) is None:
        GroupManage.files[path] = FileManage()
    return GroupManage.files[path]


def exitFile(pid, uid, path):
    fileManager = getFileManage(pid, path)
    fileManager.users[uid] -= 1
    if fileManager.users[uid] == 0:
        fileManager.users.pop(uid)
    if len(fileManager.users) == 0:
        filePath = tmppath(pid, path)
        with open(filePath, 'w') as file:
            file.write(fileManager.content)
        dockersave(pid, path)
        removetmp(pid, path)
        Manager[pid].files.pop(path)


def openFile(pid, uid, path):
    fileManager = getFileManage(pid, path)
    # print(fileManager.users)
    if len(fileManager.users) == 0:
        filepath = dockeropen(pid, path)
        with open(filepath, 'r') as file:
            fileManager.content = file.read()
    if fileManager.users.get(uid) is None:
        fileManager.users[uid] = 1
    else:
        fileManager.users[uid] += 1


def sendProjectMessage(pid, delta):
    groupManager = getGroupManage(pid)
    for channel in groupManager.channels:
        channel.send(text_data=json.dumps(delta))


def checkFileOpen(pid, path):
    if Manager.get(pid) is None:
        return True
    GroupManager = getGroupManage(pid)
    # DONE: 优化循环
    for key in GroupManager.files.keys():
        # print(key,path)
        # 最初有某种bug，因此使用前缀判断
        if key[:len(path)] == path:
            return False
    return True


def saveAllFiles(pid):
    groupManager = getGroupManage(pid)
    for path, fileManager in groupManager.files.items():
        filePath = tmppath(pid, path)
        with open(filePath, 'w') as file:
            file.write(fileManager.content)
        dockersave(pid, path)


class CoProgrammingConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()

    # DONE: 提取函数
    def addUser(self, groupManager, uid, pid):
        groupManager.group[uid] = 1
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name, {
                'type': 'coop_send',
                'delta': {
                    'type': 'memberConnect',
                    'pid': pid,
                    'uid': uid,
                    'group': list(groupManager.group.keys())
                },
                'channel_name': self.channel_name,
            })

    # DONE: 提取函数
    def popUser(self, groupManager, uid, pid):
        groupManager.group.pop(uid)
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name, {
                'type': 'coop_send',
                'delta': {
                    'type': 'memberDisconnect',
                    'pid': pid,
                    'uid': uid,
                    'group': list(groupManager.group.keys())
                },
                'channel_name': self.channel_name,
            })

    # DONE: 提取函数
    def handleInitializeTabs(self, delta):
        uid = delta.get('uid')
        pid = delta.get('pid')
        self.uid = uid
        self.pid = pid
        self.paths = []
        self.room_group_name = str(pid) + "_operation"

        # Join room group
        async_to_sync(self.channel_layer.group_add)(self.room_group_name,
                                                    self.channel_name)

        # print("connect",self.channel_name,self.uid)

        groupManager = getGroupManage(pid)
        # print('user is ',groupManager.group.get(uid))
        if groupManager.group.get(uid) is None:
            self.addUser(groupManager, uid, pid)
        else:
            groupManager.group[uid] += 1

        groupManager.channels.append(self)

        # groupManager.channelLayer = self.channel_layer

        self.send(text_data=json.dumps({
            'type': 'initializeTabs',
            'group': list(groupManager.group.keys())
        }))

    def handleInitializeFile(self, delta):
        # DONE: []换.get()
        uid = delta.get('uid')
        pid = delta.get('pid')
        path = delta.get('path')

        openFile(pid, uid, path)
        self.paths.append(path)

        fileManager = getFileManage(pid, path)
        self.send(text_data=json.dumps({
            'type': 'initializeFile',
            'content': fileManager.content,
            'revision': fileManager.revision,
            'path': path
        }))

    def handleSaveAll(self, delta):
        pid = delta.get('pid')
        # print(delta)
        saveAllFiles(pid)
        async_to_sync(self.channel_layer.group_send)(self.room_group_name, {
            'type': 'coop_send',
            'delta': {
                'type': 'saveDone',
                'pid': pid,
            },
            'channel_name': '',
        })

    def handleCloseFile(self, delta):
        pid = delta.get('pid')
        uid = delta.get('uid')
        path = delta.get('path')
        exitFile(pid, uid, path)
        self.paths.remove(path)

    def handleOperation(self, delta):
        # DONE: 去除无效变量
        # uid = delta.get('uid')
        pid = delta.get('pid')
        path = delta.get('path')
        fileManager = getFileManage(pid, path)

        if delta.get('revision') == fileManager.revision:
            fileManager.revision += 1
            applyOperation(pid, path, delta.get('operation'))
            # time.sleep(1)
            # print("send ack to",self.channel_name,"in",self.room_group_name)
            self.send(text_data=json.dumps({'type': 'ack', 'path': path}))

            # 修改转发
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name, {
                    'type': 'coop_send',
                    'delta': delta,
                    'channel_name': self.channel_name,
                })
        else:
            # print("nak delta",delta)
            # time.sleep(1)
            self.send(text_data=json.dumps({'type': 'nak', 'path': path}))
        # saveAllFiles(pid)

    def disconnect(self, close_code):
        # Leave room group
        if self.room_group_name is not None:
            # groupManager.channels.remove(self)
            uid = self.uid
            pid = self.pid

            # DONE: 去除无效代码
            # if not len(self.paths) == 0:
            for path in self.paths:
                exitFile(pid, uid, path)
            groupManager = getGroupManage(pid)
            groupManager.group[uid] -= 1
            if groupManager.group[uid] == 0:
                self.popUser(groupManager, uid, pid)

            if len(groupManager.group) == 0:
                if len(groupManager.files) != 0:
                    raise ValueError
                else:
                    Manager.pop(pid)
            # DONE: 去除无效代码
            # groupVar = groupManager.group

            async_to_sync(self.channel_layer.group_discard)(
                self.room_group_name, self.channel_name)

        # print(operateManage)

    # Receive message from WebSocket
    def receive(self, text_data):
        delta = json.loads(text_data)

        # 信号量保护
        consumerSemaphore.acquire()

        # DONE: []变为.get()
        type = delta.get('type')
        if type == 'initializeTabs':
            self.handleInitializeTabs(delta)
        elif type == 'initializeFile':
            self.handleInitializeFile(delta)
        elif type == 'saveAll':
            self.handleSaveAll(delta)
        elif type == 'closeFile':
            self.handleCloseFile(delta)
        elif type == 'operation':
            self.handleOperation(delta)

        # 释放信号量
        consumerSemaphore.release()

    # Receive message from room group
    def coop_send(self, event):
        # 不给自己发
        if self.channel_name != event['channel_name']:
            delta = event.get('delta')
            if delta.get('type') == 'operation':
                if not event.get('delta').get('path') in self.paths:
                    # 未打开，则前端请求错误，直接返回
                    # print("not send",delta['path'] , self.paths,self.channel_name)
                    return
                # print("send",delta['path'] , self.paths,self.channel_name)
            # Send message to WebSocket
            # print("send delta to",self.channel_name,"in",self.room_group_name)
            self.send(text_data=json.dumps(delta))
