from django.db import models
from django.contrib.auth.models import User
from accounts.models import Project

# Create your models here.


class ChatRecord(models.Model):
    # 如果你在定义模型时没有显式的指定主键，那么Django 会贴心的送你一个自增的 id 主键
    pid = models.ForeignKey(Project, on_delete=models.CASCADE)
    uid = models.ForeignKey(User, on_delete=models.CASCADE)
    message = models.TextField(max_length=200)
    timestamp = models.DateTimeField(auto_now_add=True)
