from click import command
from accounts.models import Project
from coconow.settings import TEMP_EDITFILE_PATH
import os


def del_files(dir_path):
    if os.path.isfile(dir_path):
        try:
            os.remove(dir_path)  # 这个可以删除单个文件，不能删除文件夹
        except BaseException as e:
            print(e)
    elif os.path.isdir(dir_path):
        file_lis = os.listdir(dir_path)
        for file_name in file_lis:
            tf = os.path.join(dir_path, file_name)
            del_files(tf)


def dockeropen(pid, path):
    dockername = Project.objects.get(id=pid).pname
    filename = tmppath(pid, path)
    command = 'docker cp '+dockername+':/home'+path+' '+filename
    os.system(command)
    # print(filename)
    # print(command)
    return filename


def dockersave(pid, path):
    dockername = Project.objects.get(id=pid).pname
    filename = tmppath(pid, path)
    command = 'docker cp '+filename+' '+dockername+':/home'+path
    os.system(command)
    # print(filename)
    # print(command)


def removetmp(pid, path):
    filename = tmppath(pid, path)
    # command='rm -f '+filename
    # os.system(command)
    # print(command)
    del_files(filename)


def tmppath(pid, path):
    return os.path.join(TEMP_EDITFILE_PATH, (str(pid)+str(path).replace('/', '_')))
