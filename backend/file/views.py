from platform import java_ver
import time
from urllib import response
from django.contrib.auth.models import User
from click import command
import paramiko
from sqlalchemy import null
from rest_framework.response import Response
from django.http import JsonResponse, FileResponse
from rest_framework.views import APIView
from accounts.models import Member, Project
from websocket_service.CoProgramming import checkFileOpen, sendProjectMessage
import json
import os
import tools

from rest_framework.authentication import SessionAuthentication, BasicAuthentication

from coconow.settings import UPLOAD_TEMPFILE_PATH


class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening

# Create your views here.


def dockerSSH(username, port, command):  # 输入端口与命令,返回stdout
    sh = paramiko.SSHClient()  # 1 创建SSH对象
    sh.set_missing_host_key_policy(
        paramiko.AutoAddPolicy())  # 2 允许连接不在know_hosts文件中的主机
    sh.connect("124.70.221.116", port=port,
               username=username, password="root")  # 3 连接服务器
    stdin, stdout, stderr = sh.exec_command(command=command)
    return stdout.read().decode("utf-8")


portBase = 20000
ResponseSuc = Response({
    "message": "success"
})
ResponseUsing = Response({
    "message": "该文件正在被使用",
    "data": {
        "detail": "该文件正在被使用"
    }
}, status=400)
ResponseNoRight = Response({
    "message": "无删除权限",
    "data": {
        "detail": "无删除权限"
    }
}, status=400)


def checkUser(user, detail):
    if not user.is_authenticated:
        return Response({
            "message": "failure",
            "data": {
                "detail": "用户未登录"
            }
        }, status=400)
    else:
        if not Member.objects.filter(pid=pk, uid=user.id).exists():
            return Response({
                "message": "failure",
                "data": {
                    "detail": detail,
                }
            }, status=400)
    return null


class FileList(APIView):
    def get(self, request, pk):
        # 判断用户登录
        user = request.user
        cU = checkUser(user, "非法访问项目")
        if cU is not null:
            return cU

        port = portBase+int(pk)
        stdout = dockerSSH("root", port, 'python /walkTree.py')
        return JsonResponse({
            'filelist': json.loads(stdout),
        })


class CreateFile(APIView):
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request, pk):
        # 判断用户登录
        user = request.user
        cU = checkUser(user, "非法访问项目")
        if cU is not null:
            return cU

        path = request.data.get("dir")
        name = request.data.get("name")
        dir = os.path.join(path, name)
        port = portBase+int(pk)
        dockerSSH("root", port, 'touch /home'+str(dir))

        sendProjectMessage(
            pk, {"type": "refresh", "message": user.username+"创建了文件"+dir, "uid": user.id})
        return ResponseSuc


class RemoveFile(APIView):
    def get(self, request, pk):
        # 判断用户登录
        user = request.user
        cU = checkUser(user, "非法删除项目")
        if cU is not null:
            return cU

        project = Project.objects.get(pk=pk)
        path = request.GET["path"]
        port = portBase+int(pk)
        if checkFileOpen(pk, path):
            if Member.objects.filter(pid=project, uid=user)[0].privilege == "owner":
                sendProjectMessage(
                    pk, {"type": "refresh", "message": user.username+"删除了文件"+path, "uid": user.id})
                dockerSSH("root", port, 'rm /home'+str(path))
            else:
                sh = paramiko.SSHClient()
                sh.set_missing_host_key_policy(
                    paramiko.AutoAddPolicy())  # 2 允许连接不在know_hosts文件中的主机
                sh.connect("124.70.221.116", port=port,
                           username=user.username, password="root")
                chan = sh.invoke_shell(term='xterm')
                stdout = chan.recv(1024)
                chan.send('rm /home'+str(path)+'\n')
                time.sleep(0.1)
                if chan.recv_ready():
                    stdout = chan.recv(1024)
                    if "write-protected" in stdout.decode() or "Operation not permitted" in stdout.decode():
                        return ResponseNoRight
                sendProjectMessage(
                    pk, {"type": "refresh", "message": user.username+"删除了文件"+path, "uid": user.id})
                return ResponseSuc

        else:
            return ResponseUsing

        sendProjectMessage(
            pk, {"type": "refresh", "message": user.username+"删除了文件"+path, "uid": user.id})
        return ResponseSuc


class CreateDir(APIView):
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request, pk):
        # 判断用户登录
        user = request.user
        cU = checkUser(user, "非法访问项目")
        if cU is not null:
            return cU

        path = request.data.get("dir")
        name = request.data.get("name")
        port = portBase+int(pk)
        dir = os.path.join(path, name)
        if Member.objects.filter(pid=pk, uid=user.id)[0].privilege == "owner":
            dockerSSH("root", port, 'mkdir /home'+str(dir))
        else:
            dockerSSH(user.username, port, 'mkdir /home'+str(dir))

        sendProjectMessage(
            pk, {"type": "refresh", "message": user.username+"创建了目录"+dir, "uid": user.id})
        return ResponseSuc


class RemoveDir(APIView):
    def get(self, request, pk):
        # 判断用户登录
        user = request.user
        cU = checkUser(user, "非法删除项目或不是拥有者")
        if cU is not null:
            return cU

        project = Project.objects.get(pk=pk)
        path = request.GET["path"]
        port = portBase+int(pk)
        if checkFileOpen(pk, path):
            if Member.objects.filter(pid=project, uid=user)[0].privilege == "owner":
                sendProjectMessage(
                    pk, {"type": "refresh", "message": user.username+"删除了目录"+path, "uid": user.id})
                dockerSSH("root", port, 'rm -rf /home'+str(path))
            else:
                sh = paramiko.SSHClient()
                sh.set_missing_host_key_policy(
                    paramiko.AutoAddPolicy())  # 2 允许连接不在know_hosts文件中的主机
                sh.connect("124.70.221.116", port=port,
                           username=user.username, password="root")
                chan = sh.invoke_shell(term='xterm')
                chan.recv(1024)
                chan.send('rm -rf /home'+str(path)+'\n')
                time.sleep(0.1)
                if chan.recv_ready():
                    stdout = chan.recv(1024)
                    if "Operation not permitted" in stdout.decode():
                        return ResponseNoRight

                sendProjectMessage(
                    pk, {"type": "refresh", "message": user.username+"删除了目录"+path, "uid": user.id})
                return ResponseSuc

        else:
            return ResponseUsing
        return ResponseSuc


class UploadFile(APIView):
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request, pk):
        # 判断用户登录
        user = request.user
        cU = checkUser(user, "非法访问")
        if cU is not null:
            return cU

        files = request.FILES.getlist('file')
        dir = request.POST.get('dir')
        dockername = Project.objects.get(id=pk).pname
        for f in files:
            file_path = os.path.join(UPLOAD_TEMPFILE_PATH, f.name)
            with open(file_path, 'wb+') as fp:
                for chunk in f.chunks():
                    fp.write(chunk)
            # command = 'docker cp '+file_path+' '+dockername+':'+'/home'+dir+'/'+f.name
            # os.system(command)
            # os.system('rm -f '+file_path)
            tools.del_files(file_path)

            port = portBase+int(pk)
            project = Project.objects.get(pk=pk)
            if Member.objects.get(uid=user, pid=project).privilege == "owner":
                dockerSSH("root", port, 'chown root ' +
                          '/home'+dir+'/'+f.name)
            else:
                dockerSSH("root", port, 'chown ' +
                          user.username+'/home'+dir+'/'+f.name)

        sendProjectMessage(
            pk, {"type": "refresh", "message": user.username+"上传了文件"+dir+'/'+f.name, "uid": user.id})
        return ResponseSuc


class DownloadFile(APIView):
    def get(self, request, pk):
        # 判断用户登录
        user = request.user
        cU = checkUser(user, "非法访问项目或不是拥有者")
        if cU is not null:
            return cU

        path = request.GET.get('path')
        dockername = Project.objects.get(id=pk).pname
        fname = os.path.basename(path)
        fpath = os.path.join(UPLOAD_TEMPFILE_PATH, fname)
        # command = 'docker cp '+dockername+':/home'+path+' '+fpath
        # os.system(command)
        ret = FileResponse(open(fpath, 'rb'), as_attachment=True)
        # os.system('rm -f '+fpath)
        tools.del_files(fpath)
        return ret
